import React from 'react';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'


const transpose = arr => {
    return Object.keys(arr[0]).map(function (c) {
      return arr.map(function (r) {
        return r[c];
      });
    });
}
  

const handleCategoryArray = data => data.map(value =>  value.category);

const handleSeriesData = (data, types, labels) => {
  
  const yValue = transpose(data.map(value => value.data.map(value => value.yValue)))
  const seriesId = transpose(data.map(value => value.data.map(value => value.seriesId))).map(value => value[0])
  const categoryId = transpose(data.map(value => value.data.map(value => value.categoryId))).map(value => value[0])

  const dataTypes = Object.keys(types);



const newArray = dataTypes.map( (value, index) => 
  {
    return {
      [value]: {
         [labels[seriesId[index]]]:[]
       }
      }
    
  })

  console.log(newArray)
  console.log(seriesId)
  // console.log(categoryId)
  return yValue.map((value, index) => {

    return {
        name: labels[seriesId[index]],
        data: value,
        stack: categoryId[index],
        // index: 1,
        // legendIndex: index+1
    }
  })
}

const handleOptions = (categoryArray, seriesData) => {

  return {
      chart: {
          type: 'bar'
      },
      title: {
          text: ''
      },
  
      xAxis: {
    
        lineWidth: 0,
        tickLength: 0,
        labels: {
        enabled: true,
        align: 'left',
        x: 0,
        y: -24
      },
      type: 'category',
      categories: categoryArray
      },
      yAxis: {
        gridLineColor: 'transparent',
        labels:{enabled: false},
        stackLabels: {
          formatter: function() {
            return this.total
          },
          enabled: true,           
          verticalAlign: 'middle',     
          align: 'right',
                  },
          min: 0,
          title: {
              text: ''
          }
      },
      credits: {
        enabled: false
      },
      exporting : {
      enabled: false
      },
      legend: {
      useHTML: true,
      labelFormatter: function(){
      
      if (this.index===1 || this.index===3)
      {
        return 'Internal: '+ this.name 
      } else {
        return 'External: ' +this.name 
      }
      
      },
        verticalAlign: 'top',
        title : {
          fontWeight: 'lighter',
        }
          
      },
      plotOptions: {
        series: {
          marker: {enabled: true},
          stacking: 'normal'
        }
      },
      series: seriesData
      // series: [{
      //     name: 'View',
      //     data: dataArrays[0],
      //     stack: 'View',
      //     color: 'red',
      //     index: 1,
      //     legendIndex: 1

      // }, {
      //     name: 'Edit',
      //     data: dataArrays[1],
      //     stack: 'View',
      //     color: '#ff6961',
      //     index: 0,
      //     legendIndex: 2
    
      // }, {
      //     name: 'View',
      //     data: dataArrays[2],
      //     stack:'Edit',
      //     index: 1,
      //     legendIndex: 3
      // }]
  }
}

const StackBarChart = props => {
  const { data, types, labels} = props;

  const categoryArray = handleCategoryArray(data)
  const seriesData = handleSeriesData(data, types, labels)
    return (
        <div style={{width: props.width, height: props.height}}>
        <HighchartsReact 
        highcharts={Highcharts}
        options={handleOptions(categoryArray, seriesData)}
        />
        </div>
       
    )
}

export default StackBarChart;