import React, { Component } from "react";
import "./App.css";
import StackBarChart from "../StackedBarChart/StackBarChart";

const barChartData = [
  {
    data: [
      {
        id: 0,
        yValue: 10,
        seriesId: "redSeries",
        typeId: "Internal"
      },
      {
        id: 1,
        yValue: 10,
        seriesId: "yellowSeries",
        typeId: "External"
      },
      {
        id: 2,
        yValue: 10,
        seriesId: "greenSeries",
        typeId: "External"
      }
    ],
    category: "bilal.shafi@emumba.com"
  },
  {
    data: [
      {
        id: 0,
        yValue: 20,
        seriesId: "redSeries",
        typeId: "Internal"
      },
      {
        id: 1,
        yValue: 10,
        seriesId: "yellowSeries",
        typeId: "External"
      },
      {
        id: 2,
        yValue: 20,
        seriesId: "greenSeries",
        typeId: "External"
      }
    ],
    category: "ahmed.waleed@emumba.com"
  },
  {
    data: [
      {
        id: 0,
        yValue: 30,
        seriesId: "redSeries",
        typeId: "Internal"
      },
      {
        id: 1,
        yValue: 30,
        seriesId: "yellowSeries",
        typeId: "External"
      },
      {
        id: 2,
        yValue: 10,
        seriesId: "greenSeries",
        typeId: "External"
      }
    ],
    category: "irfan.ali@emumba.com"
  },
  {
    data: [
      {
        id: 0,
        yValue: 30,
        seriesId: "redSeries",
        typeId: "Internal"
      },
      {
        id: 1,
        yValue: 20,
        seriesId: "yellowSeries",
        typeId: "External"
      },
      {
        id: 2,
        yValue: 10,
        seriesId: "greenSeries",
        typeId: "External"
      }
    ],
    category: "ahmed.khan@emumba.com"
  },
  {
    data: [
      {
        id: 0,
        yValue: 10,
        seriesId: "redSeries",
        typeId: "Internal"
      },
      {
        id: 1,
        yValue: 10,
        seriesId: "yellowSeries",
        typeId: "External"
      },
      {
        id: 0,
        yValue: 10,
        seriesId: "greenSeries",
        typeId: "External"
      }
    ],
    category: "fareha.kaukab@emumba.com"
  }
];

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
        </header>

        <StackBarChart
          width={800}
          height={500}
          data={barChartData}
          types={{
            internal: "Internal",
            external: "External"
          }}
          labels={{
            redSeries: "Owner",
            yellowSeries: "View",
            greenSeries: "Edit"
          }}
        />
      </div>
    );
  }
}

export default App;
